# Node API docker sample

A [Express](http://expressjs.com/) api application


## Prerequisite:

* The proyect requires [Node.js](https://nodejs.org/) v10+ or newer


## Technologies

* Express
* Node js
* Docker
* Axios


## Installation and Configuration

### 1. Dependency installation

```sh
$ cd path/of/your/project
$ npm install
```

### 2. Docker local deployment

```sh
$ cd path/of/your/project
$ docker build -t node-docker-sample:v1 .
ó
$ docker build -t <your username>/node-web-app .
```

```sh
$ cd path/of/your/project
$ docker run -p 4000:4000 -d -it node-docker-sample:v1 
ó
$ docker run -p 49160:8080 -d <your username>/node-web-app
```



## Postman Collection test
https://www.getpostman.com/collections/837e95b1aa8e1ae6fd3f