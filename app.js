require('dotenv').config();

// Develop logs api block
if(process.env.NODE_ENV === "development"){
  process.on('unhandledRejection', error => {
    console.log('unhandledRejection', error);
  });
  app.use(morgan('dev'));
}

// Library blocks
const path = require('path')
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cors = require('cors');
const server = require(path.resolve('bin', 'server'));
const { errorHandler } = require('./utils/errorHandler');
const public = require('./routes/public');


const app = express();  

app.use(bodyParser.urlencoded({
  extended: false
}));

app.use(bodyParser.json({ type: 'application/json' }));

const corsOptions = {
  'methods': ['GET', 'POST'],
  'preflightContinue': false,
  'origin': '*',
  'optionsSuccessStatus': 200
};

app.use(cors(corsOptions));

// Public routes blocks
app.use('/api', public);

// Private routes blocks

// Error handler blocks
app.use((req, res, next) => {
  const err = new Error('Not Found');
  res.status(404);
  next(err);
});

app.use(errorHandler);

module.exports = server(app);
