const server = {
    HTTP_PORT: process.env.HTTP_PORT || 3000,
    NODE_ENV: process.env.NODE_ENV,
    HOST: process.env.HOST
}

const jwt = {
    secret: process.env.JWT_SECRET,
    expiration: process.env.JWT_EXPIRATION,
    pass: process.env.JWT_SECRET
}

module.exports = {
    server: server,
    jwt: jwt
}
