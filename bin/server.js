#!/usr/bin/env node

require('dotenv').config();
const chalk = require('chalk');
const path = require('path');
const { server } = require(path.resolve('config', 'server'));

module.exports =  (app) => {
  var port = server.HTTP_PORT;
  app.listen(port, function () {
    console.log(chalk.green(`Example app listening on port: ${port}`));
  });
};


