'use strict';
const path = require('path');
const chalk = require('chalk');
const { server } = require(path.resolve('config', 'server'));

function errorHandler (err, req, res, next) {
  if(server.NODE_ENV !== "development"){
    console.log(chalk.red.underline.bold('E R R O R  H A N D L E R:'));
    console.log(chalk.green.bold('name:'), chalk.yellow.italic(err.name));
    console.log(chalk.green.bold('message:'), chalk.yellow.italic(err.message));
    console.log(chalk.green.bold('err:'), chalk.yellow.italic(err));
    console.log(chalk.green.bold('stack:'), chalk.yellow.italic(err.stack));
  }
  if (err) {
    res.statusCode = err.statusCode || 500
    let response = {
        err: {
          type: 'Error handler',
          statusCode: res.statusCode,
          name: err.name,
          message: err.message,
        }
      };
    res.send(response);
  }
  
  next();
}

module.exports = {
  errorHandler
};
