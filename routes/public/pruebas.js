'use strict';
const express = require('express');
const router = express.Router();
const chuckController = require('../../controllers/chuckController');

router.get('/chuckJoke', async (req, res, next) => {
  try {
    const resp = await chuckController.chuckJokes();
    res.send(resp);
  }
  catch (e) {
    e.statusCode = 500;
    next(e)
  }
    
});


module.exports = router;
