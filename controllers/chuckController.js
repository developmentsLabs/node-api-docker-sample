
const axios = require('axios')
const qs = require('qs');

async function chuckJokes() {
    try {

        const resRamdomJokes = await getRandomJokes()

        return {
            data: resRamdomJokes
        };
    }
    catch (error) {
        return {
            success: false,
            message: error.stack
        };
    }
}

async function getRandomJokes() {
    try {


        const url = 'https://matchilling-chuck-norris-jokes-v1.p.rapidapi.com/jokes/random';
        const options = {
            method: 'get',
            headers: {
                "X-RapidAPI-Host": "matchilling-chuck-norris-jokes-v1.p.rapidapi.com",
                "X-RapidAPI-Key": "f657bfbf9emshe435e867167844fp1d45b1jsn2078f688dbc8",
                "accept": "application/json"
            },
            url,
        };

        return axios(options).then((response) => {

            return response.data;
        
        }).catch((error) => {
            throw error
        });

        // return resChuckJokes
    } catch (error) {
        throw error
    }
}

module.exports = {
    chuckJokes
};
